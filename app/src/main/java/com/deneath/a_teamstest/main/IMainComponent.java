package com.deneath.a_teamstest.main;

import dagger.Component;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */
@Component(modules = MainModule.class)
public interface IMainComponent {
    void inject(MainActivity activity);
}
