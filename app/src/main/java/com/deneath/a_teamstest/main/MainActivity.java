package com.deneath.a_teamstest.main;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.deneath.a_teamstest.R;
import com.deneath.a_teamstest.contacts.ContactsFragment;
import com.deneath.a_teamstest.dagger.App;
import com.deneath.a_teamstest.jsontest.JsonTestFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements IMainView {

    private JsonTestFragment mJsonTestFragment;
    private ContactsFragment mContactsFragment;

    @BindView(R.id.bottomBar)
    AHBottomNavigation mBottomBar;

    @Inject
    MainPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        App.getApplication(this).getMainComponent().inject(this);
        ButterKnife.bind(this);
        mPresenter.setView(this);

        mPresenter.onCreate();
    }

    @Override
    public void initViews() {
        mJsonTestFragment = JsonTestFragment.createInstance();
        mContactsFragment = ContactsFragment.createInstance();

        AHBottomNavigationItem item1 = new AHBottomNavigationItem(getString(R.string.json_test_label), R.drawable.note);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(getString(R.string.contacts_label), R.drawable.contacts);
        mBottomBar.addItem(item1);
        mBottomBar.addItem(item2);
        mBottomBar.setInactiveColor(ContextCompat.getColor(this, R.color.secondaryText));
        mBottomBar.setAccentColor(ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.setOnTabSelectedListener((position, wasSelected) -> {
            mPresenter.tabSelected(position);
            return true;
        });
        mBottomBar.setCurrentItem(0);
    }

    @Override
    public void showJsonTestScreen() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.containerLayout, mJsonTestFragment);
        transaction.commit();
    }

    @Override
    public void showContactsScreen() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.containerLayout, mContactsFragment);
        transaction.commit();
    }
}
