package com.deneath.a_teamstest.main;

import com.deneath.a_teamstest.jsontest.JsonTestFragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */
@Module
class MainModule {
    @Provides
    MainPresenter providePresenter(){return new MainPresenter();}
}
