package com.deneath.a_teamstest.main;

import com.deneath.a_teamstest.infrastructure.BasePresenter;

import javax.inject.Inject;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 05.10.2017.
 */

public class MainPresenter extends BasePresenter<IMainView> {

    @Inject
    MainPresenter() {
    }

    void onCreate() {
        View.initViews();
    }

    void tabSelected(int position) {
        switch (position) {
            case 0:
                View.showJsonTestScreen();
                break;
            case 1:
                View.showContactsScreen();
                break;
            default: throw new IllegalArgumentException("Invalid position");
        }
    }
}
