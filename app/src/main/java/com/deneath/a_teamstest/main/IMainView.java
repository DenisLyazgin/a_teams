package com.deneath.a_teamstest.main;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 05.10.2017.
 */

public interface IMainView {

    void initViews();

    void showJsonTestScreen();

    void showContactsScreen();
}
