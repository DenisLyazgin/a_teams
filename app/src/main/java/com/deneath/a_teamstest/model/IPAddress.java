package com.deneath.a_teamstest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */

public class IPAddress {
    @SerializedName("ip")
    @Expose
    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
