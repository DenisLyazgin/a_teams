package com.deneath.a_teamstest.service;

import com.deneath.a_teamstest.helpers.GsonIPDeserializer;
import com.deneath.a_teamstest.model.IPAddress;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */

public class ApiService implements IApiService {

    private final String REQUIRED_IP_URL = "http://ip.jsontest.com/";
    private final String WORKING_IP_URL = "https://api.ipify.org?format=json";



    private Gson mGson;
    private OkHttpClient mHttpClient;

    @Inject
    public ApiService(OkHttpClient okHttpClient, GsonBuilder gsonBuilder) {
        gsonBuilder.registerTypeAdapter(IPAddress.class, new GsonIPDeserializer());
        mGson = gsonBuilder.create();
        mHttpClient = okHttpClient;
    }

    @Override
    public Observable<IPAddress> getIp() {
        return Observable.defer(() -> {
            try {
                Response response = mHttpClient.newCall(new Request.Builder().url(WORKING_IP_URL).build()).execute();
                if(response.isSuccessful()){
                    IPAddress result = mGson.fromJson(response.body().string(), IPAddress.class);
                    return Observable.just(result);
                }
                return Observable.error(new IllegalStateException(response.message()));
            } catch (IOException e) {
                return Observable.error(e);
            }
        });
    }

    @Override
    public Observable<IPAddress> getTime() {
        return null;
    }
}
