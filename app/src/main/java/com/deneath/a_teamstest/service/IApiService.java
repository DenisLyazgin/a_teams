package com.deneath.a_teamstest.service;

import com.deneath.a_teamstest.model.IPAddress;

import rx.Observable;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */

public interface IApiService {
    Observable<IPAddress> getIp();
    Observable<IPAddress> getTime();
}
