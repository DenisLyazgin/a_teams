package com.deneath.a_teamstest.jsontest;

import dagger.Component;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */
@Component(modules = JsonTestModule.class)
public interface IJsonTestComponent {
    void inject(JsonTestFragment fragment);
}
