package com.deneath.a_teamstest.jsontest;

import com.deneath.a_teamstest.service.ApiService;
import com.deneath.a_teamstest.service.IApiService;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */
@Module
class JsonTestModule {
    @Provides
    JsonTestPresenter providePresenter(IApiService apiService) {
        return new JsonTestPresenter(apiService);
    }

    @Provides
    IApiService provideApiService(OkHttpClient client, GsonBuilder builder){
        return new ApiService(client, builder);
    }

    @Provides
    OkHttpClient provideOkHttpClient(){
        return new OkHttpClient();
    }

    @Provides
    GsonBuilder provideGsonBuilder(){
        return new GsonBuilder();
    }
}
