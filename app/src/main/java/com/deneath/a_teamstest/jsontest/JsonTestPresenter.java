package com.deneath.a_teamstest.jsontest;

import com.deneath.a_teamstest.infrastructure.BasePresenter;
import com.deneath.a_teamstest.service.IApiService;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */

class JsonTestPresenter extends BasePresenter<IJsonTestView> {

    private IApiService mApiService;

    @Inject
    JsonTestPresenter(IApiService apiService) {
        mApiService = apiService;
    }

    @Inject
    void onCreate() {
        mApiService.getIp()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(ipAddress -> View.showIp(ipAddress.getIp()), throwable -> View.showMessage(throwable.getMessage()));

        View.initViews();
    }
}
