package com.deneath.a_teamstest.jsontest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.deneath.a_teamstest.R;
import com.deneath.a_teamstest.dagger.App;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */

public class JsonTestFragment extends Fragment implements IJsonTestView{

    @BindView(R.id.ipAddressTextView)
    TextView mIpAddressTextView;

    @Inject
    JsonTestPresenter Presenter;

    public static JsonTestFragment createInstance(){
        return new JsonTestFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_json_test, container, false);

        App.getApplication(getActivity()).getJsonTestComponent().inject(this);
        ButterKnife.bind(this, view);

        Presenter.setView(this);

        Presenter.onCreate();
        return view;
    }

    @Override
    public void initViews() {

    }

    @Override
    public void showIp(String ip) {
        mIpAddressTextView.setText(ip);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }
}
