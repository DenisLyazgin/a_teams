package com.deneath.a_teamstest.jsontest;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */

public interface IJsonTestView {
    void initViews();

    void showIp(String ip);

    void showMessage(String message);
}

