package com.deneath.a_teamstest.dagger;

import com.deneath.a_teamstest.main.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 05.10.2017.
 */
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(MainActivity activity);
}
