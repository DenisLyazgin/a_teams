package com.deneath.a_teamstest.dagger;

import android.app.Application;
import android.content.Context;

import com.deneath.a_teamstest.jsontest.DaggerIJsonTestComponent;
import com.deneath.a_teamstest.jsontest.IJsonTestComponent;
import com.deneath.a_teamstest.main.DaggerIMainComponent;
import com.deneath.a_teamstest.main.IMainComponent;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 05.10.2017.
 */

public class App extends Application {
    private static Context mContext;
    private AppComponent appComponent;
    private IMainComponent mMainComponent;
    private IJsonTestComponent mJsonTestComponent;

    public static App getApplication(Context context) {
        return (App) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
    public IMainComponent getMainComponent(){return mMainComponent;}
    public IJsonTestComponent getJsonTestComponent(){return mJsonTestComponent;}

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        appComponent = DaggerAppComponent.create();
        mMainComponent = DaggerIMainComponent.create();
        mJsonTestComponent = DaggerIJsonTestComponent.create();
    }
}
