package com.deneath.a_teamstest.contacts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.deneath.a_teamstest.R;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */

public class ContactsFragment extends Fragment {

    public static ContactsFragment createInstance(){
        return new ContactsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }
}
