package com.deneath.a_teamstest.infrastructure;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */

public class BasePresenter<T> {
    protected T View;
    public void setView(T view){View = view;}
}
