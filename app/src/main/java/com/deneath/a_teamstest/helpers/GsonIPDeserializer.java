package com.deneath.a_teamstest.helpers;

import com.deneath.a_teamstest.model.IPAddress;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Denis Lyazgin(lyazgindenis@gmail.com) on 06.10.2017.
 */

public class GsonIPDeserializer implements JsonDeserializer<IPAddress> {
    @Override
    public IPAddress deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();

        final String ipString = jsonObject.get("ip").getAsString();

        final IPAddress ip = new IPAddress();
        ip.setIp(ipString);
        return ip;
    }
}
